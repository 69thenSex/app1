// App1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include<iostream>
#include<vector> 
using namespace std; 

float priceCalculate(float,float,float,float,float);
float discountCalculate(float);
  
int main() 
{ 
    vector<float> bookShelf; 
    int choice;
  
	cout<<"========================================Ken's book shop========================================"<<endl;
	do{
		float width,height,weight,dayInStock;
		cout<<"Input your scale type (0) for centimeters or (1) for inches (-1) for go next step"<<endl;
		cin>>choice;
		if(choice != -1){
			if(choice == 0){
				//Default input in centimeters and pounds.
				cout<<"Input width of book "<<bookShelf.size()<<" (centimeters)"<<endl;
				cin>>width;
				cout<<"Input height of book "<<bookShelf.size()<<" (centimeters)"<<endl;
				cin>>height;
				cout<<"Input weight of book "<<bookShelf.size()<<" (pounds)"<<endl;
				cin>>weight;
				cout<<"Input day it has been in stock"<<endl;
				cin>>dayInStock;
				cout<<endl<<endl;
			}else{
				//input inches, and a set of scales in grams!
				cout<<"Input width of book "<<bookShelf.size()<<" (inches)"<<endl;
				cin>>width;
				cout<<"Input height of book "<<bookShelf.size()<<" (inches)"<<endl;
				cin>>height;
				cout<<"Input weight of book "<<bookShelf.size()<<" (grams)"<<endl;
				cin>>weight;
				cout<<"Input day it has been in stock"<<endl;
				cin>>dayInStock;
				cout<<endl<<endl;
			}
			//insert to Vector
			float price = priceCalculate(width,height,weight,dayInStock,choice);
			if(price > 0) bookShelf.push_back(price);
			else bookShelf.push_back(0);
		}
	}while(choice != -1);
	
	cout<<endl<<endl<<"=======================================PRICE LIST OF BOOK======================================="<<endl;
	for(int i = 0 ; i< bookShelf.size();i++){
		cout<<"BOOK ID : "<<i<<" PRICE : "<<bookShelf[i] <<" Bath"<<endl;
	}
	
	do{
		float totalPrice=0,bookQuantity=0,minPrice;
		do{
			int bookID;
			cout<<"Buy book ID : ";
			cin>>bookID;
			if(bookID > bookShelf.size() || bookID < 0) cout<<"Invalid book ID"<<endl;
			else {
				if(totalPrice == 0) minPrice = bookShelf[bookID];
				if(minPrice > bookShelf[bookID]) minPrice = bookShelf[bookID];
				totalPrice += bookShelf[bookID];
				bookQuantity++;
			}
			cout<<"Do you want any book? (0) yes (-1) no : ";
			cin>>choice;
		}while(choice != -1);
		
		int member;
		float discount;
		
		cout<<"Customer is a memebership (0) for has (1) for not : ";
		cin>>member;
		if(member == 0 && bookQuantity >= 3) discount = minPrice*0.2;
		else discount = 0;
		cout<<"PRICE : "<<totalPrice<<"  Bath"<<endl;
		cout<<"DISCOUNT : "<<discount<<"  Bath"<<endl;
		cout<<"TOTAL PRICE : "<<totalPrice - discount<<"  Bath"<<endl;
		cout<<"Calculate another customer? (0) yes  (-1) no : ";
		cin>>choice;
	}while(choice != -1);
    return 0; 
} 

//Price = surface area of book in square centimeters * weight of the book in pounds * 10 Baht – Discount
float priceCalculate(float width,float height,float weight,float days,float option){
	const float cmToInches = 2.54 ,poundToGram = 453.59237;
	if(option == 0) return (width * height * weight * 10) - discountCalculate(days);
	else return ((width * cmToInches) * (height * cmToInches) * (weight / poundToGram) * 10) - discountCalculate(days);
}

//Discount = 1Baht per day it has been in stock over 10 days, and 2 Baht per day it has been in stock over 30 days
float discountCalculate(float days){
	if(days > 10){
		if(days > 30) return days*2;
		else return days;
	}else return 0;
}

